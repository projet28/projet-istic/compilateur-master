package TP2.ASD;

import java.util.ArrayList;
import java.util.List;

import TP2.Llvm;
import TP2.Llvm.IR;
import TP2.SymbolException;
import TP2.SymbolTable;
import TP2.TypeException;
import TP2.ASD.ProtoFonction.Argument;
import TP2.SymbolTable.FunctionSymbol;
import TP2.SymbolTable.VariableSymbol;

public class DefFonction extends Fonction {
	Type type;
	String nom;
	List<Argument> s;
	Instruction i;
	
	public DefFonction(Type type, String nom,List<Argument> s,Instruction i) {
		this.type=type;
		this.nom=nom;
		this.s=s;
		this.i=i;
	}
	
	@Override
	public String pp() {
		String str = "FUNC " + type.pp() + " " + nom + "(";
		if(!s.isEmpty()) {
			str += s.get(0).pp();
			for(Argument s1:s.subList(1, s.size())) {
				str += ", " + s1.pp(); 
			}
		}
		
		return str + ")\n" + i.pp(1);
	}

	@Override
	public Llvm.IR toIR(SymbolTable st) throws TypeException, SymbolException {
		if(st.lookup(nom) != null) {
			if(!(st.lookup(nom) instanceof FunctionSymbol)) {
				 throw new SymbolException("the symbol : " + nom + " is not a function");
			}
			if(((FunctionSymbol)st.lookup(nom)).defined) {
				throw new SymbolException("the symbol already exist and defined: " + nom);
			}
		}
		
		Llvm.IR ir = new Llvm.IR(Llvm.empty(), Llvm.empty());
		
		SymbolTable ste = new SymbolTable(st);
		
		List<Llvm.Type> ts = new ArrayList<Llvm.Type>();
		
		List<String> ss = new ArrayList<String>();
		
		if(((FunctionSymbol)st.lookup(nom)) != null && !((FunctionSymbol)st.lookup(nom)).defined) {
			
			List<VariableSymbol> argProto = ((FunctionSymbol)st.lookup(nom)).arguments;
			
			if(s.size() != argProto.size()) {
				throw new SymbolException("the function : " + nom + " as not the same number of argument as the proto");
			}
			
			for(int i=0; i<s.size(); i++) {
				if(!argProto.get(i).type.equals(s.get(i).t))
					throw new SymbolException("the function : " + nom + " as the " + s.get(i).nom + 
							"variable not the same type of the proto " + s.get(i).t.pp() + " but " + 
							argProto.get(i).type + " expected");
					
				VariableSymbol v = new VariableSymbol(s.get(i).t, s.get(i).nom);
					
				ste.add(v);
				ts.add(new Int().toLlvmType());
				ss.add(s.get(i).nom);
			}
			
			((FunctionSymbol)st.lookup(nom)).defined = true;
			
		}else {
			List<VariableSymbol> arguments = new ArrayList<VariableSymbol>();
			
			for(Argument s1:s) {
				VariableSymbol v = new VariableSymbol(s1.t,s1.nom);
				
				arguments.add(v);
				ste.add(v);
				ts.add(new Int().toLlvmType());
				ss.add(s1.nom);
			}
			st.add(new FunctionSymbol(type, nom, arguments, true));
		}
		
		Llvm.Instruction def = new Llvm.Define(type.toLlvmType(), nom, ts, ss);
		
		IR instructionIr = i.toIR(ste);
		
		Llvm.Instruction bd = new Llvm.BD();
		
		ir.appendCode(def);
		ir.append(instructionIr);
		
		if(type instanceof Void) {
			Llvm.Instruction v = new Llvm.ReturnVoid();
			ir.appendCode(v);
		}else {
			Llvm.Instruction v = new Llvm.Return(new Int().toLlvmType(), "0");
			ir.appendCode(v);
		}
		ir.appendCode(bd);
		
		return ir;
	}		
}
