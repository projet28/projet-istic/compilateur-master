package TP2.ASD;

import TP2.Llvm;

public class Array extends Type {
	public int taille;
	
	public Array() {
		taille = -1;
	}
	
	public Array(int taille) {
		this.taille = taille;
	}
	
    public String pp() {
    	return "ARRAY";
    }

    @Override public boolean equals(Object obj) {
      return obj instanceof Array;
    }

    public Llvm.Type toLlvmType() {
      return new Llvm.Array(taille);
    }
  }
