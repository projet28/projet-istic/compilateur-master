package TP2.ASD;

import TP2.Llvm;
import TP2.SymbolException;
import TP2.SymbolTable;
import TP2.TypeException;

public abstract class Instruction {
	public abstract String pp(int indent);
	public abstract Llvm.IR toIR(SymbolTable st) throws TypeException, SymbolException;
  }
