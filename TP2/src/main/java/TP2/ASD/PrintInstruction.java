package TP2.ASD;

import java.util.ArrayList;
import java.util.List;

import TP2.Llvm;
import TP2.SymbolException;
import TP2.SymbolTable;
import TP2.TypeException;
import TP2.Utils;
import TP2.Utils.LLVMStringConstant;

public class PrintInstruction extends Instruction{
	
	static public class Item {
		Expression e;
		String cdc;
		boolean isExp;
		
		public Item(Expression e) {
			this.e = e;
			isExp = true;
		}
		
		public Item(String cdc) {
			this.cdc = cdc;
			isExp = false;
		}
		
		String pp() {
			if(isExp) {
				return e.pp();
			}else {
				return "\"" + cdc + "\"";
			}
		}
	}
	
	List<Item> l;

	public PrintInstruction(List<Item> l) {
		this.l = l;
	}
	
	@Override
	public String pp(int indent) {
		String str =  "";
		if(!l.isEmpty()) {
			str += Utils.indent(indent) + "PRINT " + l.get(0).pp();
			for(Item i: l.subList(1, l.size())) {
				str += ", " + i.pp();
			}
		}
		
		return str;
	}

	@Override
	public Llvm.IR toIR(SymbolTable st) throws TypeException, SymbolException {
		Llvm.IR ir = new Llvm.IR(Llvm.empty(), Llvm.empty());
		
		String glob = Utils.newglob("@.fmt");
		String mess = "";
		List<String> tmps = new ArrayList<String>();
		for(Item i: l) {
			if(i.isExp) {
				mess += "%d";
				tmps.add(i.e.toIR(st).result);
			}else {
				mess += i.cdc;
			}
		}
		LLVMStringConstant llvmMess = Utils.stringTransform(mess);
		Llvm.Instruction printG = new Llvm.GlobalVar(glob, llvmMess.length, llvmMess.str);
		ir.appendHeader(printG);
		
		Llvm.Instruction print = new Llvm.Print(glob, llvmMess.length, tmps);
		ir.appendCode(print);
		
		return ir;
	}

}
