package TP2.ASD;

import java.util.ArrayList;
import java.util.List;

import TP2.Llvm;
import TP2.SymbolException;
import TP2.SymbolTable;
import TP2.TypeException;
import TP2.Utils;
import TP2.SymbolTable.FunctionSymbol;

public class AppelFonctionExp extends Expression {
	String nom;
	List<Expression> e;
	
	public AppelFonctionExp(String nom,List<Expression> e) {
		this.nom=nom;
		this.e = e;
	}
	
	@Override
	public String pp() {
		String s = nom + "(" ;
		if(!e.isEmpty()) {
			s += e.get(0).pp();
			for(Expression e2: e.subList(1, e.size())) {
				s += ", " + e2.pp();
			}
		}
		
		return s+= ")";
	}

	@Override
	public RetExpression toIR(SymbolTable st) throws TypeException, SymbolException {
		if(st.lookup(nom) == null) {
			throw new SymbolException("the symbol doesn't exist : " + nom);
		}
		if(!(st.lookup(nom) instanceof FunctionSymbol)) {
			 throw new SymbolException("the symbol : " + nom + " is not a function");
		 }
		
		String tmp = Utils.newtmp();
		
		Type type = ((FunctionSymbol)st.lookup(nom)).returnType;
		
		Llvm.IR ir = new Llvm.IR(Llvm.empty(), Llvm.empty());
		
		List<Llvm.Type> ts = new ArrayList<Llvm.Type>();
		List<String> vs = new ArrayList<String>();
		
		for(Expression e2: e) {
			RetExpression ret = e2.toIR(st);
			ir.append(ret.ir);
			ts.add(ret.type.toLlvmType());
			vs.add(ret.result);
		}
		
		Llvm.Instruction appel = new Llvm.CallExp(tmp, type.toLlvmType(), nom, ts, vs);
	
		ir.appendCode(appel);
		
		return new RetExpression(ir, type, tmp);
	}		
}
