package TP2.ASD;

import java.util.List;

import TP2.Llvm;
import TP2.SymbolException;
import TP2.SymbolTable;
import TP2.SymbolTable.FunctionSymbol;
import TP2.SymbolTable.VariableSymbol;
import TP2.TypeException;

public class Program {
	List<Fonction> f;

    public Program(List<Fonction> f) {
    	this.f = f;
    }

    // Pretty-printer
    public String pp() {
    	String str = "";
    	for(Fonction f2: f) {
    		str += f2.pp();
    	}
        return str;
    }

    // IR generation
    public Llvm.IR toIR() throws TypeException, SymbolException {
      SymbolTable st = new SymbolTable();
      
      Llvm.IR ir = new Llvm.IR(Llvm.empty(), Llvm.empty());
      
      for(Fonction f2: f) {
    	  ir.append(f2.toIR(st));
      }
      if(st.lookup("main") == null) {
    	  throw new SymbolException("there is no main function");
      }
      if(st.lookup("main") instanceof VariableSymbol) {
    	  throw new SymbolException("main is not a function");
      }
      if(!((FunctionSymbol) st.lookup("main")).arguments.isEmpty()) {
    	  throw new SymbolException("the main function has parameter");
      }

      return ir;
    }
  }