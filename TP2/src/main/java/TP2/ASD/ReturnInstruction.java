package TP2.ASD;

import TP2.Llvm;
import TP2.SymbolException;
import TP2.SymbolTable;
import TP2.TypeException;
import TP2.Utils;
import TP2.ASD.Expression.RetExpression;

public class ReturnInstruction extends Instruction{
	
	Expression e;
	
	public ReturnInstruction(Expression e) {
		this.e = e;
	}

	@Override
	public String pp(int indent) {
		return  Utils.indent(indent) + "RETURN " + e.pp() + "\n";
	}

	@Override
	public Llvm.IR toIR(SymbolTable st) throws TypeException, SymbolException {
		RetExpression ret = e.toIR(st);
		
		Llvm.Instruction retour = new Llvm.Return(ret.type.toLlvmType(), ret.result);
		
		ret.ir.appendCode(retour);
		
		return ret.ir;
	}

}
