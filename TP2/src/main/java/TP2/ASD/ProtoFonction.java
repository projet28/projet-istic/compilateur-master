package TP2.ASD;

import java.util.ArrayList;
import java.util.List;

import TP2.Llvm;
import TP2.SymbolException;
import TP2.SymbolTable;
import TP2.TypeException;
import TP2.SymbolTable.FunctionSymbol;
import TP2.SymbolTable.VariableSymbol;

public class ProtoFonction  extends Fonction{
	
	static public class Argument{
		public String nom;
		public Type t;
		
		public Argument(String nom, Type t) {
			this.nom = nom;
			this.t = t;
		}
		
		public String pp() {
			if(t instanceof Array) {
				return nom+"[]";
			}else {
				return nom;
			}
		}
	}

	Type type;
	String nom;
	List<Argument> s;
	
	public ProtoFonction(Type type, String nom,List<Argument> s) {
		this.type=type;
		this.nom=nom;
		this.s=s;
	}
	
	@Override
	public String pp() {
		String str = "PROTO " + type.pp() + " " + nom + "(";
		if(!s.isEmpty()) {
			str += s.get(0).pp();
			for(Argument s1:s.subList(1, s.size())) {
				str += ", " + s1.pp(); 
			}
		}
		
		return str + ")\n";
	}

	@Override
	public Llvm.IR toIR(SymbolTable st) throws TypeException, SymbolException {
		if(st.lookup(nom) != null) {
			throw new SymbolException("the symbol already exist : " + nom);
		}
			
		Llvm.IR ir = new Llvm.IR(Llvm.empty(), Llvm.empty());
		List<VariableSymbol> arguments = new ArrayList<VariableSymbol>();
		
		for(Argument s1:s) {
			VariableSymbol v = new VariableSymbol(s1.t, s1.nom);
			
			arguments.add(v);
		}
		st.add(new FunctionSymbol(type, nom, arguments, false));
		
		return ir;
	}

}
