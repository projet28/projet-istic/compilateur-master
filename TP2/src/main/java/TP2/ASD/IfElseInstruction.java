package TP2.ASD;

import TP2.Llvm;
import TP2.SymbolException;
import TP2.SymbolTable;
import TP2.TypeException;
import TP2.Utils;
import TP2.ASD.Expression.RetExpression;

public class IfElseInstruction extends Instruction {
	Expression e;
	Instruction i1;
	Instruction i2;
	
	public IfElseInstruction(Expression e,Instruction i1,Instruction i2) {
		this.e=e;
		this.i1=i1;
		this.i2=i2;
	}
	
	@Override
	public String pp(int indent) {
		return  Utils.indent(indent) + "IF " + e.pp() + "\n" +  Utils.indent(indent) + "THEN" + "\n" +
	    Utils.indent(indent) + i1.pp(indent+1) + "\n" +  Utils.indent(indent) + 
	    "ELSE" + "\n" + Utils.indent(indent) + i2.pp(indent+1) + "\n" +  Utils.indent(indent) + "FI" ;
	}

	@Override
	public Llvm.IR toIR(SymbolTable st) throws TypeException, SymbolException {
		RetExpression ret = e.toIR(st);
		String result = ret.result;
		Type type = ret.type;
		
		String label1 = Utils.newlab("IfEqual");
		String label2 = Utils.newlab("IfUnequal");
		String label3 = Utils.newlab("fi");
		
		String tmp = Utils.newtmp();
		
		Llvm.Instruction cmp = new Llvm.icmp(tmp,type.toLlvmType(),result);
		Llvm.Instruction cond = new Llvm.BrCond(new Llvm.Bool(), tmp, label1, label2);
		Llvm.Instruction l1 = new Llvm.Label(label1);
		Llvm.Instruction fi = new Llvm.BrIncond(label3);
		Llvm.Instruction l2 = new Llvm.Label(label2);
		Llvm.Instruction l3 = new Llvm.Label(label3);
		
		ret.ir.appendCode(cmp);
		ret.ir.appendCode(cond);
		
		ret.ir.appendCode(l1);
		ret.ir.append(i1.toIR(st));
		
		ret.ir.appendCode(fi);
		
		ret.ir.appendCode(l2);
		ret.ir.append(i2.toIR(st));
		
		ret.ir.appendCode(fi);
		
		ret.ir.appendCode(l3);
		
		return ret.ir;
	}

}
