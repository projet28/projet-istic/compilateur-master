package TP2.ASD;

import TP2.*;
import TP2.ASD.Expression.RetExpression;
import TP2.SymbolTable.VariableSymbol;

public class AffectInstruction extends Instruction {
	String ident;
    Expression e;
    Expression t;

    public AffectInstruction(String ident, Expression e) {
      this.ident = ident;
      this.e = e;
    }
    
    public AffectInstruction(String ident, Expression t, Expression e) {
        this.ident = ident;
        this.t = t;
        this.e = e;
      }

    // Pretty-printer
    public String pp(int indent) {
    	if(t != null) {
    		return Utils.indent(indent) + ident + "[" + t.pp() + "] := " + e.pp();
    	}else {
    		return Utils.indent(indent) + ident + " := " + e.pp();
    	}
    }

	@Override
	public Llvm.IR toIR(SymbolTable st) throws TypeException, SymbolException {
		RetExpression eRet = e.toIR(st);

		if(st.lookup(ident) == null) {
			throw new SymbolException("the symbol doesn't exist : " + ident);
		}
		
		if(!(st.lookup(ident) instanceof VariableSymbol))
			throw new SymbolException(ident + "not a variable");
		
		if(t != null) {
			RetExpression tRet = t.toIR(st);
			
			if(!(eRet.type instanceof Int)) {
		        throw new TypeException("type mismatch: have " + eRet.type + " but need Int");
		      }
			
			String tmp = Utils.newtmp();
			
			Llvm.Instruction tab = new Llvm.Tab(eRet.type.toLlvmType(), tRet.result, tmp, ident);
			Llvm.Instruction affect = new Llvm.Affect(eRet.type.toLlvmType(), eRet.result, tmp);
			
			eRet.ir.append(tRet.ir);
		    eRet.ir.appendCode(tab);
		    eRet.ir.appendCode(affect);
		}else {
		      if(!((VariableSymbol)st.lookup(ident)).type.equals(eRet.type)) {
		        throw new TypeException("type mismatch: " + ident + " have " + ((VariableSymbol)st.lookup(ident)).type.pp() + " and need " + eRet.type.pp());
		      }
		      
		      Llvm.Instruction affect = new Llvm.Affect(eRet.type.toLlvmType(), eRet.result, "%"+ident);
		      eRet.ir.appendCode(affect);
		}
		
	      return eRet.ir;
	}
}
