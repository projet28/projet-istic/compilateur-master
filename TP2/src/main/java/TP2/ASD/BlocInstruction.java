package TP2.ASD;


import java.util.List;

import TP2.Llvm;
import TP2.SymbolException;
import TP2.SymbolTable;
import TP2.TypeException;
import TP2.Utils;

public class BlocInstruction extends Instruction{
	Declas d;
	List<Instruction> i;
	
	public BlocInstruction(Declas d, List<Instruction> i) {
		this.d = d;
		this.i = i;
	}
	
	@Override
	public String pp(int indent) {
		String res =  Utils.indent(indent-1) + "{\n";
		res += d.pp(indent);
		res += "\n";
		for(Instruction i2 : i) {
			res += i2.pp(indent) + "\n";
		}
		return res +  Utils.indent(indent-1) + "}\n";
	}

	@Override
	public Llvm.IR toIR(SymbolTable st) throws TypeException, SymbolException {
		SymbolTable st2 = new SymbolTable(st);
		
		Llvm.IR ir = d.toIR(st2);
		for(Instruction i2 : i) {
			ir.append(i2.toIR(st2));
		}
		return ir;
	}

}
