package TP2.ASD;

import TP2.Llvm;
import TP2.SymbolException;
import TP2.SymbolTable;
import TP2.Utils;
import TP2.SymbolTable.VariableSymbol;
import TP2.TypeException;

public class VarExpression extends Expression {
 String nom;
 Expression t;
 
 public VarExpression(String nom) {
   this.nom = nom;
 }
 
 public VarExpression(String nom, Expression t) {
	   this.nom = nom;
	   this.t = t;
	 }

 public String pp() {
	 if(t != null) 
		 return nom + "[" + t.pp() + "]";
	 else
		 return nom;
 }

 public RetExpression toIR(SymbolTable st) throws SymbolException, TypeException {
	 if(st.lookup(nom) == null) {
			throw new SymbolException("the symbol doesn't exist : " + nom);
		}
	 if(!(st.lookup(nom) instanceof VariableSymbol)) {
		 throw new SymbolException("the symbol : " + nom + " is not a variable");
	 }
	 
	 String tmp = Utils.newtmp();
	 Type type = ((VariableSymbol)st.lookup(nom)).type;
	 
	 if(t != null) {
		 RetExpression tRet = t.toIR(st);
		 
		 String tmp2 = Utils.newtmp();
		 
		 Llvm.Instruction tab = new Llvm.Tab(type.toLlvmType(), tRet.result, tmp, nom);
		 Llvm.Instruction var = new Llvm.Var(tmp2, tmp, type.toLlvmType());
		
		 tRet.ir.appendCode(tab);
		 tRet.ir.appendCode(var);
		 return new RetExpression(tRet.ir, new Int(), tmp2);
	 }else {
		 Llvm.Instruction var = new Llvm.Var(tmp, "%"+nom, type.toLlvmType());
		 Llvm.IR ir = new Llvm.IR(Llvm.empty(), Llvm.empty());
		 ir.appendCode(var);
		 return new RetExpression(ir, type, tmp);
	 }
 }
}
