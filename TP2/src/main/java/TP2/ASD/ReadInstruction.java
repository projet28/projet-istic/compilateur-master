package TP2.ASD;

import java.util.ArrayList;
import java.util.List;

import TP2.Llvm;
import TP2.SymbolException;
import TP2.SymbolTable;
import TP2.SymbolTable.VariableSymbol;
import TP2.Utils.LLVMStringConstant;
import TP2.ASD.Expression.RetExpression;
import TP2.TypeException;
import TP2.Utils;

public class ReadInstruction extends Instruction{
	
	static public class Item {
		Expression e;
		String nom;
		Type t;
		
		public Item(String nom, Type t, Expression e) {
			this.nom = nom;
			this.e = e;
			this.t = t;
		}
		
		public Item(String nom, Type t) {
			this.nom = nom;
			this.t = t;
		}
		
		String pp() {
			if(t instanceof Array) {
				return nom + "[" + e.pp() + "]";
			}else {
				return nom;
			}
		}
	}
	
	List<Item> l;

	public ReadInstruction(List<Item> l) {
		this.l = l;
	}
	
	@Override
	public String pp(int indent) {
		String str =  Utils.indent(indent) + "READ ";
		if(!l.isEmpty()) str += l.get(0).pp();
		for(Item s: l.subList(1, l.size())) {
			str += ", " + s.pp();
		}
		return str;
	}

	@Override
	public Llvm.IR toIR(SymbolTable st) throws TypeException, SymbolException {
		Llvm.IR ir = new Llvm.IR(Llvm.empty(), Llvm.empty());
		
		String glob = Utils.newglob("@.fmt");
		String mess = "";
		List<String> tmps = new ArrayList<String>();
		
		
		
		for(Item s: l) {
			if(st.lookup(s.nom) == null) {
				throw new SymbolException("the symbol doesn't exist : " + s);
			}
			if(!(st.lookup(s.nom) instanceof VariableSymbol)) {
				throw new SymbolException("the symbol : " + s + " is not a variable");
			}
			
			if(s.t instanceof Array) {
				RetExpression eRet = s.e.toIR(st);
				
				String tmp = Utils.newtmp();
				
				Llvm.Instruction tab = new Llvm.Tab(eRet.type.toLlvmType(), eRet.result, tmp, s.nom);
				ir.append(eRet.ir);
			    ir.appendCode(tab);
				
				mess += "%d";
				tmps.add(tmp);
			}else {
				mess += "%d";
				tmps.add(s.nom);
			}
		}
		LLVMStringConstant llvmMess = Utils.stringTransform(mess);
		Llvm.Instruction readG = new Llvm.GlobalVar(glob, llvmMess.length, llvmMess.str);
		ir.appendHeader(readG);
		
		Llvm.Instruction read = new Llvm.Read(glob, llvmMess.length, tmps);
		ir.appendCode(read);
		
		return ir;
	}

}
