package TP2.ASD;

import java.util.List;

import TP2.*; 
import TP2.SymbolTable.Symbol;
import TP2.SymbolTable.VariableSymbol;

public class Declas{
	List<Decla> l;

    public Declas(List<Decla> l) {
      this.l = l;
    }

    // Pretty-printer
    public String pp(int indent) {
      String str = "";
      if(!l.isEmpty()) {
    	  str +=  Utils.indent(indent) + "INT " + l.get(0).pp();
    	  for(Decla d: l.subList(1, l.size())) {
        	  str += ", " + d.pp();
          }
      }
      
      return str;
    }

	public Llvm.IR toIR(SymbolTable st) throws TypeException, SymbolException {

		Llvm.IR ir = new Llvm.IR(Llvm.empty(), Llvm.empty());
		
		for(Decla d: l) {
			Symbol s = new VariableSymbol(d.t, d.ident);
	        if(!st.add(s)) throw new SymbolException("symbol already exist : " + d.ident);
			
	        Llvm.Instruction decla = new Llvm.Decla(d.t.toLlvmType(), d.ident);
	        ir.appendCode(decla);
		}
		
	    return ir;
	}
}
