package TP2.ASD;

import TP2.Llvm;
import TP2.SymbolException;
import TP2.SymbolTable;
import TP2.TypeException;
import TP2.Utils;
import TP2.ASD.Expression.RetExpression;

public class WhileInstruction extends Instruction {
	Expression e;
	Instruction i;
	
	public WhileInstruction(Expression e,Instruction i) {
		this.e=e;
		this.i=i;
	}
	
	@Override
	public String pp(int indent) {
		return  Utils.indent(indent) + "WHILE " + e.pp() + "\n" + Utils.indent(indent) + "DO\n " +
				 Utils.indent(indent) + i.pp(indent+1) + "\n" + Utils.indent(indent) + "DONE\n" ;
	}

	@Override
	public Llvm.IR toIR(SymbolTable st) throws TypeException, SymbolException {
		Llvm.IR ir = new Llvm.IR(Llvm.empty(), Llvm.empty());
		
		String label1 = Utils.newlab("while");
		String label2 = Utils.newlab("Do");
		String label3 = Utils.newlab("Done");
		
		String tmp = Utils.newtmp();
		
		RetExpression rete = e.toIR(st);
		String result = rete.result;
		Type type = rete.type;
		
		Llvm.Instruction br = new Llvm.BrIncond(label1);
		Llvm.Instruction l1 = new Llvm.Label(label1);
		Llvm.Instruction cmp = new Llvm.icmp(tmp,type.toLlvmType(),result);
		Llvm.Instruction cond = new Llvm.BrCond(new Llvm.Bool(), tmp, label2, label3);
		Llvm.Instruction l2 = new Llvm.Label(label2);
		Llvm.Instruction l3 = new Llvm.Label(label3);
		
		ir.appendCode(br);
		ir.appendCode(l1);
		
		ir.append(rete.ir);
		ir.appendCode(cmp);
		
		ir.appendCode(cond);
		
		
		ir.appendCode(l2);
		ir.append(i.toIR(st));
		
		ir.appendCode(br);
		
		
		ir.appendCode(l3);
		
		return ir;
	}

}
