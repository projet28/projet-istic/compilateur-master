package TP2.ASD;

public class Decla{
	public String ident;
    public Type t;
    
	public Decla(String ident, Type t) {
		this.ident = ident;
	    this.t =t;
	}
	
	public String pp() {
		if(t instanceof Array)
			return ident + "[" + ((Array)t).taille + "]";
		else
			return ident;
	}
}
