package TP2;

import java.util.List;
import java.util.ArrayList;

// This file contains a simple LLVM IR representation
// and methods to generate its string representation

public class Llvm {
  static public class IR {
    List<Instruction> header; // IR instructions to be placed before the code (global definitions)
    List<Instruction> code;   // main code

    public IR(List<Instruction> header, List<Instruction> code) {
      this.header = header;
      this.code = code;
    }

    // append an other IR
    public IR append(IR other) {
      header.addAll(other.header);
      code.addAll(other.code);
      return this;
    }

    // append a code instruction
    public IR appendCode(Instruction inst) {
      code.add(inst);
      return this;
    }

    // append a code header
    public IR appendHeader(Instruction inst) {
      header.add(inst);
      return this;
    }

    // Final string generation
    public String toString() {
      // This header describe to LLVM the target
      // and declare the external function printf
      StringBuilder r = new StringBuilder("; Target\n" +
        "target triple = \"x86_64-unknown-linux-gnu\"\n" +
        "; External declaration of the printf function\n" +
        "declare i32 @printf(i8* noalias nocapture, ...)\n" +
        "\n; Actual code begins\n\n");

      for(Instruction inst: header)
        r.append(inst);

      r.append("\n\n");


      for(Instruction inst: code)
        r.append(inst);

      return r.toString();
    }
  }

  // Returns a new empty list of instruction, handy
  static public List<Instruction> empty() {
    return new ArrayList<Instruction>();
  }


  // LLVM Types
  static public abstract class Type {
    public abstract String toString();
  }
  
  static public class Array extends Type {
	  int taille;
	  
	  public Array(int taille) {
		  this.taille = taille;
	  }
	  
	    public String toString() {
	      return "[" + taille + " x " + new Int() + "]";
	    }
	  }
  
  static public class Void extends Type {
	    public String toString() {
	      return "void";
	    }
	  }
  
  static public class Word extends Type {
	    public String toString() {
	      return "i64";
	    }
	  }

  static public class Int extends Type {
    public String toString() {
      return "i32";
    }
  }
  
  static public class Char extends Type {
	    public String toString() {
	      return "i8";
	    }
	  }
  
  static public class Bool extends Type {
	    public String toString() {
	      return "i1";
	    }
	  }


  // LLVM IR Instructions
  static public abstract class Instruction {
    public abstract String toString();
  }
  
  static public abstract class Expression extends Instruction {
	  Type type;
	    String left;
	    String right;
	    String lvalue;

	    public Expression(Type type, String left, String right, String lvalue) {
	      this.type = type;
	      this.left = left;
	      this.right = right;
	      this.lvalue = lvalue;
	    }
  }

  static public class Add extends Expression {

    public Add(Type type, String left, String right, String lvalue) {
      super(type, left, right, lvalue);
    }

    public String toString() {
      return "  " + lvalue + " = add " + type + " " + left + ", " + right +  "\n";
    }
  }
  
  static public class Minus extends Expression {
	    
	    public Minus(Type type, String left, String right, String lvalue) {
	    	super(type, left, right, lvalue);
	    }

	    public String toString() {
	      return "  " + lvalue + " = sub " + type + " " + left + ", " + right +  "\n";
	    }
	  }
  
  static public class Mul extends Expression {
	    
	    public Mul(Type type, String left, String right, String lvalue) {
	    	super(type, left, right, lvalue);
	    }

	    public String toString() {
	      return "  " + lvalue + " = mul " + type + " " + left + ", " + right +  "\n";
	    }
	  }
  
  static public class Div extends Expression {
	    
	    public Div(Type type, String left, String right, String lvalue) {
	    	super(type, left, right, lvalue);
	    }

	    public String toString() {
	      return "  " + lvalue + " = udiv " + type + " " + left + ", " + right +  "\n";
	    }
	  }
  
  static public class Affect extends Instruction {
	    Type type;
	    String exp;
	    String nom;

	    public Affect(Type type, String exp, String nom) {
	      this.type = type;
	      this.exp = exp;
	      this.nom = nom;
	    }

	    public String toString() {
	      return "  store " + type + " " + exp + ", " + type + "* " + nom +  "\n";
	    }
	  }
  
  static public class Tab extends Instruction {
	    Type type;
	    String exp1;
	    String tmp;
	    String nom;

	    public Tab(Type type, String exp1, String tmp, String nom) {
	      this.type = type;
	      this.exp1 = exp1;
	      this.tmp = tmp;
	      this.nom = nom;
	    }

	    public String toString() {
	    	return "  " + tmp + " = getelementptr " + type + ", " + type +
	    			"* %" + nom + ", " + new Word() + " 0, " + new Int() + " " + exp1 + "\n";
	    }
	  }
  
  static public class Decla extends Instruction {
	    Type type;
	    String nom;

	    public Decla(Type type, String nom) {
	      this.type = type;
	      this.nom = nom;
	    }

	    public String toString() {
	      return "  %" + nom + " = alloca " + type + "\n";
	    }
	  }
  
  static public class icmp extends Instruction {
	    String tmp;
	  	Type type;
	  	String exp;
	    public icmp(String tmp, Type type, String exp) {
	      this.tmp = tmp;
	      this.type = type;
	      this.exp = exp;
	    }

	    public String toString() {
	      return "  " + tmp + " = icmp ne " + type + " " + exp + " , 0\n";
	    }
	  }
  
  static public class BrCond extends Instruction {
	  	Type type;
	    String tmp;
	  	String label1;
	  	String label2;
	    public BrCond(Type type, String tmp,String label1, String label2) {
	      this.type = type;
	      this.tmp = tmp;
	      this.label1 = label1;
	      this.label2 = label2;
	    }

	    public String toString() {
	      return "  br " + type + " " + tmp + ", label %" + label1 + ", " + "label %" + label2 + "\n";
	    }
	  }
  
  static public class BrIncond extends Instruction {
	    String label1;
	  	
	    public BrIncond(String label1) {
	      this.label1 = label1;
	    }

	    public String toString() {
	      return "  br " + "%" + label1 + "\n";
	    }
	  }
  
  
  static public class Label extends Instruction {
	    String label;

	    public Label(String label) {
	      this.label = label;
	    }

	    public String toString() {
	      return label + " : \n";
	    }
	  }
  
  static public class Var extends Instruction {
	    String tmp;
	    String nom;
	    Type type;

	    public Var(String tmp, String nom, Type type) {
	      this.tmp = tmp;
	      this.nom = nom;
	      this.type = type;
	    }

	    public String toString() {
	      return "  " + tmp + " = load " + type + ", " + type + "* " + nom +  "\n";
	    }
	  }
  
  static public class CallExp extends Instruction {
	  String tmp;
	  Type type;
	  String nom;
	  List<Type> types;
	  List<String> vars;
	  
	  public CallExp(String tmp, Type type, String nom, List<Type> types, List<String> vars) {
		  this.tmp = tmp;
		  this.type = type;
		  this.nom = nom;
		  this.types = types;
		  this.vars = vars;
	}
	  
	  public String toString() {
		  String s = "  " + tmp + " = call " + type + " @" + nom + " (";
		  if(!types.isEmpty()) s += types.get(0) + " " + vars.get(0);
		  for(int i = 1; i<types.size(); i++) {
			  s += ", " + types.get(i) + " " + vars.get(i);
		  }
		  return s + ")\n";
	  }
	  
   }
  
  static public class CallVoid extends Instruction {
	  String nom;
	  List<Type> types;
	  List<String> vars;
	  
	  public CallVoid(String nom, List<Type> types, List<String> vars) {
		  this.nom = nom;
		  this.types = types;
		  this.vars = vars;
	}
	  
	  public String toString() {
		  String s = "  call void @" + nom + " (";
		  if(!types.isEmpty()) s += types.get(0) + " " + vars.get(0);
		  for(int i = 1; i<types.size(); i++) {
			  s += ", " + types.get(i) + " " + vars.get(i);
		  }
		  return s + ")\n";
	  }
	  
   }
  
  static public class Define extends Instruction {
	  Type type;
	  String nom;
	  List<Type> types;
	  List<String> vars;
	  
	  public Define(Type type, String nom, List<Type> types, List<String> vars) {
		  this.type = type;
		  this.nom = nom;
		  this.types = types;
		  this.vars = vars;
	  }
	  
	  public String toString() {
		  String s = "define " + type + " @" + nom + " (";
		  if(!types.isEmpty()) s += types.get(0) + " %" + vars.get(0);
		  for(int i = 1; i<types.size(); i++) {
			  s += ", " + types.get(i) + " %" + vars.get(i);
		  }
		  return s + ") {\n";
	  }
  }
  
  static public class BD extends Instruction {
	  public String toString() {
		  return "}\n";
	  }
  }
  
  static public class GlobalVar extends Instruction {
	  String glob;
	  int nbChar;
	  String mess;
	  
	  public GlobalVar(String glob, int nbChar, String mess) {
		  this.glob = glob;
		  this.nbChar = nbChar;
		  this.mess = mess;
	  }
	  
	  public String toString() {
		  return glob + " = global [" + nbChar + " x " + new Char() + "] c\"" + mess + "\"\n";
	  }
  }
  
  static public class Print extends Instruction {
	  String glob;
	  int nbChar;
	  List<String> tmps;
	  
	  public Print(String glob, int nbChar, List<String> tmps) {
		  this.glob = glob;
		  this.nbChar = nbChar;
		  this.tmps = tmps;
	  }
	  
	  public String toString() {
		  String s = "  call " + new Int() + "(" + new Char() + "*, ...) @printf(" + new Char() +
				  "* getelementptr inbounds ([" + nbChar + " x " + new Char() + "], [" +
				  nbChar + " x " + new Char() + "]* " + glob + ", " + new Word() + " 0, " +
				  new Word() + " 0)";
		  for(String s2: tmps) {
			  s += ", " + new Int() + " " + s2;
		  }
		  return s + ")\n";
	  }
  }
  
  static public class Read extends Instruction {
	  String glob;
	  int nbChar;
	  List<String> tmps;
	  
	  public Read(String glob, int nbChar, List<String> tmps) {
		  this.glob = glob;
		  this.nbChar = nbChar;
		  this.tmps = tmps;
	  }
	  
	  public String toString() {
		  String s = "  call " + new Int() + "(" + new Char() + "*, ...) @scanf(" + new Char() +
				  "* getelementptr inbounds ([" + nbChar + " x " + new Char() + "], [" +
				  nbChar + " x " + new Char() + "]* " + glob + ", " + new Word() + " 0, " +
				  new Word() + " 0)";
		  for(String s2: tmps) {
			  s += ", " + new Int() + "* " + s2;
		  }
		  return s + ")\n";
	  }
  }

  static public class Return extends Instruction {
    Type type;
    String value;

    public Return(Type type, String value) {
      this.type = type;
      this.value = value;
    }

    public String toString() {
      return "  ret " + type + " " + value + "\n";
    }
  }
  
  static public class ReturnVoid extends Instruction {
	    public String toString() {
	      return "  ret " + new Void() + "\n";
	    }
	  }

}
