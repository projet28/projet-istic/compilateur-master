parser grammar VSLParser;

options {
  language = Java;
  tokenVocab = VSLLexer;
}

@header {
  package TP2;

  import java.util.stream.Collectors;
  import java.util.Arrays;
  import java.util.List;
  import java.util.ArrayList;
}

program returns [TP2.ASD.Program out]
    : {List<TP2.ASD.Fonction> l = new ArrayList<TP2.ASD.Fonction>();}
      (f = fonction {l.add($f.out);})+ EOF 
      { $out = new TP2.ASD.Program(l); }
    ;
    

typeFunc returns [TP2.ASD.Type out]
	: INT { $out = new TP2.ASD.Int(); } |
	  VOID { $out = new TP2.ASD.Void(); }
	;
	
param returns [TP2.ASD.ProtoFonction.Argument out]
	: s=IDENT {$out = new TP2.ASD.ProtoFonction.Argument($s.text, new TP2.ASD.Int());}
	| s=IDENT LC RC {$out = new TP2.ASD.ProtoFonction.Argument($s.text, new TP2.ASD.Array());}
    ;
    
fonction returns [TP2.ASD.Fonction out]
	: PROTO t=typeFunc l=IDENT LP { List<TP2.ASD.ProtoFonction.Argument> li = new ArrayList<TP2.ASD.ProtoFonction.Argument>();}
          (p=param {li.add($p.out);} (VIR p=param {li.add($p.out);})*)?  RP
          {$out = new TP2.ASD.ProtoFonction($t.out, $l.text, li);}
    | FUNC t=typeFunc l=IDENT LP { List<TP2.ASD.ProtoFonction.Argument> li = new ArrayList<TP2.ASD.ProtoFonction.Argument>();}
      (p=param {li.add($p.out);} (VIR p=param {li.add($p.out);})*)?  RP
      i=instruction
      {$out = new TP2.ASD.DefFonction($t.out, $l.text, li, $i.out);}
	;

declas returns [TP2.ASD.Declas out]
	: {List<TP2.ASD.Decla> ld = new ArrayList<TP2.ASD.Decla>();} 
	  (INT d=decla {ld.add($d.out);} (VIR d=decla {ld.add($d.out);})*)?
	  { $out = new TP2.ASD.Declas(ld); }
	;

decla returns [TP2.ASD.Decla out]
	: l=IDENT { $out = new TP2.ASD.Decla($l.text, new TP2.ASD.Int()); }
	| l=IDENT LC i=INTEGER RC { $out = new TP2.ASD.Decla($l.text, new TP2.ASD.Array($i.int)); }
	;

instruction returns [TP2.ASD.Instruction out]
	: RETURN e=expression { $out = new TP2.ASD.ReturnInstruction($e.out); }
	| l=IDENT LC t=expression RC AFFECT e=expression  
	  { $out = new TP2.ASD.AffectInstruction($l.text, $t.out, $e.out); }
	| l=IDENT AFFECT e=expression  { $out = new TP2.ASD.AffectInstruction($l.text, $e.out); }
	| IF e=expression THEN i1=instruction ELSE i2=instruction FI
	  {$out = new TP2.ASD.IfElseInstruction($e.out, $i1.out, $i2.out); }
	| IF e=expression THEN i1=instruction FI
	  {$out = new TP2.ASD.IfInstruction($e.out, $i1.out); }
	| WHILE e=expression DO i=instruction DONE { $out = new TP2.ASD.WhileInstruction($e.out, $i.out); }  
	| l=IDENT LP { List<TP2.ASD.Expression> li = new ArrayList<TP2.ASD.Expression>();}
          (e=expression {li.add($e.out);} (VIR e=expression {li.add($e.out);})*)?  RP
          {$out = new TP2.ASD.AppelFonctionVoid($l.text, li);}
	| PRINT { List<TP2.ASD.PrintInstruction.Item> li = new ArrayList<TP2.ASD.PrintInstruction.Item>(); }
	  itp=itemP {li.add($itp.out);} (VIR itp=itemP {li.add($itp.out);})*
	  { $out = new TP2.ASD.PrintInstruction(li); }
	| READ { List<TP2.ASD.ReadInstruction.Item> li = new ArrayList<TP2.ASD.ReadInstruction.Item>(); }
	  itr=itemR {li.add($itr.out);} (VIR itr=itemR {li.add($itr.out);})*
	  { $out = new TP2.ASD.ReadInstruction(li); }
	| LB {List<TP2.ASD.Instruction> li = new ArrayList<TP2.ASD.Instruction>();}
	  d=declas (i=instruction {li.add($i.out);} )+ RB
	  { $out = new TP2.ASD.BlocInstruction($d.out, li); }
	;

itemP returns [TP2.ASD.PrintInstruction.Item out]
	: e=expression { $out = new TP2.ASD.PrintInstruction.Item($e.out); }
	| s=TEXT { $out = new TP2.ASD.PrintInstruction.Item($s.text); } 
	;
	
itemR returns [TP2.ASD.ReadInstruction.Item out]
	: s=IDENT LC e=expression RC { $out = new TP2.ASD.ReadInstruction.Item($s.text, new TP2.ASD.Array(), $e.out); }
	| s=IDENT { $out = new TP2.ASD.ReadInstruction.Item($s.text, new TP2.ASD.Int()); } 
	;


expression returns [TP2.ASD.Expression out]
    : l=expression MINUS r=expression  { $out = new TP2.ASD.MinusExpression($l.out, $r.out); }
    | l=expression PLUS r=expression  { $out = new TP2.ASD.AddExpression($l.out, $r.out); }
    | f=factor { $out = $f.out; }
    ;

factor returns [TP2.ASD.Expression out]
    : l=factor MUL r=factor  { $out = new TP2.ASD.MulExpression($l.out, $r.out); }
    | l=factor DIV r=factor  { $out = new TP2.ASD.DivExpression($l.out, $r.out); }
    | p=primary { $out = $p.out; }
    ;

primary returns [TP2.ASD.Expression out]
    : INTEGER { $out = new TP2.ASD.IntegerExpression($INTEGER.int); }
    | l=IDENT LP { List<TP2.ASD.Expression> li = new ArrayList<TP2.ASD.Expression>();}
      (e=expression {li.add($e.out);} (VIR e=expression {li.add($e.out);})*)?  RP
      {$out = new TP2.ASD.AppelFonctionExp($l.text, li);}
    | l=IDENT { $out = new TP2.ASD.VarExpression($l.text); }
    | l=IDENT LC e=expression RC { $out = new TP2.ASD.VarExpression($l.text, $e.out); }
    | LP r=expression RP { $out = $r.out; }
    ;
